module no.ntnu.mappe3 {
    requires transitive javafx.controls;
    requires javafx.fxml;

    opens no.ntnu.mappe3.controller to javafx.fxml;
    exports no.ntnu.mappe3;
}