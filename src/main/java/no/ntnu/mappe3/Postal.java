package no.ntnu.mappe3;

import java.util.Objects;
import no.ntnu.mappe3.exception.PostalCodeException;

/**
 * Represents a postal code, which has an associated postal office and municipality
 */
public class Postal {
    private String postalCode;
    private String postalOffice;
    private String municipality;

    /**
     * Creates an instance of {@link Postal}
     * @param postalCode    the postal code
     * @param postalOffice  the postal office
     * @param municipality  the municipality
     * @throws PostalCodeException      if postal code is invalid
     * @throws IllegalArgumentException if postal office or municipality is blank or null
     */
    public Postal(String postalCode, String postalOffice, String municipality) throws PostalCodeException, IllegalArgumentException {
        this.isValidPostalCode(postalCode);
        this.checkForNullAndBlank(postalOffice);
        this.checkForNullAndBlank(municipality);

        this.postalCode = postalCode;
        this.postalOffice = postalOffice;
        this.municipality = municipality;
    }

    public String getPostalCode() {
        return postalCode;
    }
    public String getPostalOffice() {
        return postalOffice;
    }
    public String getMunicipality() {
        return municipality;
    }

    /**
     * Checks if postal code is valid (a four-digit code).
     * <p>
     * Norwegian Postal Codes are 4-digit codes.
     * The numbers start at {@code 00} and increase with the distance from the capital city.
     * The lowest post code in use is {@code 0001}, and the highest is {@code 9991}.
     * @param postalCode
     * @throws PostalCodeException if the postal code is not valid
     */
    public void isValidPostalCode(String postalCode) throws PostalCodeException {
        if (postalCode.isEmpty() || postalCode.length() != 4) {
            throw new PostalCodeException("'" + postalCode + "' does not contain exactly 4 digits");
        } else {
            for (int i = 0; i < postalCode.length(); i++) {
                if (!Character.isDigit(postalCode.charAt(i))) {
                    throw new PostalCodeException("'" + postalCode + "' contains characters other than digits");
        }}}
    }

    /**
     * Checks the parameter against {@code null} and blank string.
     * @param param the String to check
     * @throws IllegalArgumentException if the argument is null or blank
     */
    private void checkForNullAndBlank(String param) throws IllegalArgumentException {
        if (param == null) {
            throw new IllegalArgumentException("Parameter cannot be null");
        }
        if (param.isBlank()) {
            throw new IllegalArgumentException("Parameter cannot be blank");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {return true;}
        if (obj == null || getClass() != obj.getClass()) {return false;}

        Postal that = (Postal) obj;
        return postalCode.equals(that.postalCode) && postalOffice.equals(that.postalOffice) && municipality.equals(that.municipality);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postalCode, postalOffice, municipality);
    }

    @Override
    public String toString() {
        return (getPostalCode() + "\t" + getPostalOffice() + "\t" + getMunicipality());
    }
}