package no.ntnu.mappe3;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Represents a register containing all Norwegian postal codes
 */
public class PostalRegister {
    /**
     * Name of file in resources folder. 
     * This file contains a tab separated table of all postal codes and their municipalities
     */
    private static final String FILE = "Postnummerregister-UTF.txt";
    private static final String DELIMITER = "\t";
    private ArrayList<Postal> postals;

    /**
     * Create new {@link PostalRegister} and fills the register with imported postal codes from {@code FILE}.
     */
    public PostalRegister() {
        this.postals = new ArrayList<Postal>();
        importPostals();
    }

    public ArrayList<Postal> getPostals() {
        return postals;
    }

    /**
     * Reads data from {@code FILE} and adds new {@link Postal}s to register.
     * <p>
     * The {@code FILE} must contain values for postal code, postal office and municipality seperated by {@code DELIMITER}
     */
    private void importPostals() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.getClass().getResourceAsStream("/" + FILE)));
            // Store each line in file as a string
            String line;
            while ((line = reader.readLine()) != null) {
                // Split the string by each delimiter
                String[] data = line.split(DELIMITER);
                // Try to make new objects from data
                try {this.postals.add(new Postal(data[0], data[1], data[3]));}
                catch (Exception e) {System.err.println("[ERROR] " + e.getMessage());}
            }
            reader.close();
        } catch (Exception e) {
            System.err.println("[ERROR] " + e.getMessage());
        }
    }

    @Override
    public String toString() {
        String string = "";
        for (Postal postal : postals) {
            string += postal.toString() + "\n";
        }
        return string;
    }
}
