package no.ntnu.mappe3.exception;

/**
 * Exception for when a postal code is invalid
 */
public class PostalCodeException extends Exception {
    public PostalCodeException(String message) {
        super("Postal code is not valid: " + message);
    }
}
