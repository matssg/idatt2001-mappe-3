package no.ntnu.mappe3;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * This is the main class of the application.
 * Loads FXML files and shows stages.
 */
public class App extends Application {
    public static PostalRegister register = new PostalRegister();
    private static Scene scene;

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Postal Register");
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/images/logo.png")));
        scene = new Scene(loadFXML("main"));
        stage.setScene(scene);
        stage.show();
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/" + fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }
}