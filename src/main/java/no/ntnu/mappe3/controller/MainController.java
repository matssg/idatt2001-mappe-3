package no.ntnu.mappe3.controller;

import no.ntnu.mappe3.*;
import javafx.fxml.FXML;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

/**
 * The controller for {@code main.fxml}
 */
public class MainController {
    // ▼ Searchbar
    @FXML private TextField searchBar;
    @FXML private Label searchMatches;
    // ▼ Table
    @FXML private TableView<Postal> tableView;
    @FXML private TableColumn<Postal, String> postalCodeColumn, postalOfficeColumn, municipalityColumn;

    // Store table data in observable list so that is can be filtered, sorted and shown using JavaFX
    private final ObservableList<Postal> register = FXCollections.observableList(App.register.getPostals());

    /**
     * Called to initialize a controller after its root element has been completely processed.
     */
    public void initialize() {
        // Connect columns to values
        postalCodeColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPostalCode()));
        postalOfficeColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getPostalOffice()));
        municipalityColumn.setCellValueFactory(cellData -> new ReadOnlyStringWrapper(cellData.getValue().getMunicipality()));

        // Wrap register in a FilteredList
        // Set it to initially display all postal codes
        FilteredList<Postal> filteredRegister = new FilteredList<>(register, p -> true);

        // Set predicate for whenever the input changes
        searchBar.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredRegister.setPredicate(postal -> {
                // If searchbar is empty > display all postal codes
                if (newValue == null || newValue.isEmpty()) {return true;}

                // Check if input is part of any postal code or municipaly
                // Checks if input is part of any other string using .indexOf()
                String input = newValue.toLowerCase();
                if (postal.getPostalCode().toLowerCase().indexOf(input) != -1 ) {return true;} 
                else if (postal.getPostalOffice().toLowerCase().indexOf(input) != -1) {return true;} 
                else if (postal.getMunicipality().toLowerCase().indexOf(input) != -1) {return true;} 
                else {return false;}
            });
            // Display amount of results when search bar is used
            if (filteredRegister.isEmpty()) {
                searchMatches.setTextFill(Color.RED);
                searchMatches.setText("0 results");
            } else {
                searchMatches.setTextFill(Color.BLACK);
                searchMatches.setText((filteredRegister.size()) + " results");
            }
        });

        // Wrap the filteredRegister in a SortedList and bind to tableView
        SortedList<Postal> sortedRegister = new SortedList<>(filteredRegister);
        sortedRegister.comparatorProperty().bind(tableView.comparatorProperty());

        // Add sorted (and filtered) data to table
        tableView.setItems(sortedRegister);
    }
}
