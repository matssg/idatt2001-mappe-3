package no.ntnu.mappe3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class PostalRegisterTest {
    
    @Test
    @DisplayName("Create an instance of PostalRegister")
    public void test_createInstance() {
        PostalRegister register = new PostalRegister();
        Assertions.assertEquals(5132, register.getPostals().size());
    }
}
