package no.ntnu.mappe3;

import no.ntnu.mappe3.exception.PostalCodeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;

public class PostalTest {

    @Test
    @DisplayName("Create an instance of Postal - Valid input")
    public void test_createInstance_validParameters() throws PostalCodeException {
        Postal postal = new Postal("6600", "SUNNDALSØRA", "SUNNDAL");

        Assertions.assertEquals("6600", postal.getPostalCode());
        Assertions.assertEquals("SUNNDALSØRA", postal.getPostalOffice());
        Assertions.assertEquals("SUNNDAL", postal.getMunicipality());
    }

    @Test
    @DisplayName("Create an instance of Postal - Invalid postal code (has too many digits)")
    public void _invalidParameters_postalCode_null() {
        Exception exception = Assertions.assertThrows(PostalCodeException.class, () -> {new Postal("12345", "SUNNDALSØRA", "SUNNDAL");});
    
        String expectedMessage = "does not contain exactly 4 digits";
        String actualMessage = exception.getMessage();
    
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Create an instance of Postal - Invalid postal code (contains letters)")
    public void _invalidParameters_postalCode_characters() {
        Exception exception = Assertions.assertThrows(PostalCodeException.class, () -> {new Postal("6B00", "SUNNDALSØRA", "SUNNDAL");});
        
        String expectedMessage = "contains characters other than digits";
        String actualMessage = exception.getMessage();
        
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Create an instance of Postal - Invalid postal office (is null)")
    public void _invalidParameters_postalOffice() {
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {new Postal("6600", null, "SUNNDAL");});
        
        String expectedMessage = "Parameter cannot be null";
        String actualMessage = exception.getMessage();
    
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Create an instance of Postal - Invalid municipality (is blank)")
    public void _invalidParameters_municipality() {
        Exception exception = Assertions.assertThrows(IllegalArgumentException.class, () -> {new Postal("6600", "SUNNDALSØRA", "");});
        
        String expectedMessage = "Parameter cannot be blank";
        String actualMessage = exception.getMessage();
        
        Assertions.assertTrue(actualMessage.contains(expectedMessage));
    }
}
